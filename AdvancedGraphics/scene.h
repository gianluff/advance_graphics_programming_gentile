#pragma once
#include <vector>
#include <string>
#include <iostream>

#include "transform.h"
#include "shapes3d.h"
#include "camera.h"

namespace mgd{

class GameObj{
public:
    Transform transform;

    // qui  la descrizione del contenuto: una mesh, un collider, etc TUTTO ESPRESSO IN SPAZIO LOCALE
    // Per adesso, tutti i gameObjects sono... sfere con dei nasi
    Sphere body, nose;

    GameObj():
        transform(),
        body(Vector3(0,1,0),1),
        nose(Vector3(0,1,0.3f),0.7)
    {
    }
};

class Scene{
public:
    std::vector<GameObj> obj; // a set with GameObj (each with its own transform)

    void populate(int n){
        for (int i=0;i<n;i++){
            GameObj someoneNew;
            someoneNew.transform.translate = Vector3::random(5) + Vector3(0,0,15);
            someoneNew.transform.translate.y = 1;
            someoneNew.transform.rotate = Quaternion::fromVector3(-Vector3(0,0,1));
            obj.push_back(  someoneNew );
        }
    }

    // produces a vector of spheres in world space
    std::vector<Sphere> toWorld() const{
        std::vector<Sphere> res;
        res.clear();
        
        for (const GameObj &g : obj) {
            res.push_back( apply( g.transform , g.nose ) );
            res.push_back( apply( g.transform , g.body ) );
        }
        return res;
    }

    std::vector<Sphere> toView(Transform camera) const {
        std::vector<Sphere> res;
        res.clear();
        for (const GameObj& g : obj) {
            if (!areEqual(g.transform, camera)) {
                res.push_back(apply(camera.inverse(), apply(g.transform, g.nose)));
                res.push_back(apply(camera.inverse(), apply(g.transform, g.body)));
            }
        }
        return res;
    }
};

// ascii art: convert an intensity value (0 to 1) into a sequence of two chars
const char* intensityToCstr(Scalar intensity){
    switch( int (round(intensity*8 ))) {
    case 0: return "  "; // darkest
    case 1: return " '";
    case 2: return " +";
    case 3: return " *";
    case 4: return " #";
    case 5: return "'#";
    case 6: return "+#";
    case 7: return "*#";
    case 8: return "##"; // lightest
    default:
    case 10: return "##";
    }
}


const char* lighting( Versor3 normal ){
    Versor3 lightDir = Versor3(1,2,-5).normalized();
    // lambertian
    Scalar diffuse = dot( normal, lightDir );
    if (diffuse < 0) diffuse = 0;

    return intensityToCstr( diffuse );
}

void rayCasting(const std::vector<Sphere>& sphereVector){

    Camera c(3.0 , 50,50);

    Plane aPlane( Point3(0,-1.5,0), Versor3(0,1,0) );

    std::string screenBuffer ; // a string to get ready and print all at once

    for (int y = 0; y<c.pixelDimY; y++) {
        for (int x= 0; x<c.pixelDimX; x++) {
            Point3 hitPos;
            Point3 hitNorm;
            Scalar distMax = 1000.0;

            for  (Sphere s : sphereVector) {
               rayCast( c.primaryRay(x,y)  , s, hitPos , hitNorm , distMax );
            }

            rayCast( c.primaryRay(x,y)  , aPlane  , hitPos , hitNorm , distMax );   
            screenBuffer += lighting( hitNorm );
        }
        screenBuffer +=  "\n";
    }
    std::cout << screenBuffer;
}


} // end of namespace mgd

