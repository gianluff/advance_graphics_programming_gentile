#include <iostream>
#include <chrono>
#include <conio.h>

#include "vector3.h"
#include "shapes3d.h"
#include "camera.h"

#include "quaternion.h"
#include "transform.h"

#include "scene.h"


using namespace mgd;

// enumeratore utilizzato per distinguere le due possibili viste
enum CameraState {
    CameraView,
    CharacterView
};

void unitTestLinearOps(){

    Vector3 a(24,1,32), b(12,3,54);
    Scalar k = 5.0;
    assert( areEqual( a+b , b+a ) );
    assert( areEqual( (a+b)*k , b*k + a*k ) );
}

void unitTestProdutcs(){
    Vector3 a(24,-3,32), b(12,3,-54), c(10,13,-43);
    Scalar k = 0.4f;

    assert( isZero( dot( a , cross(a,b) ) ) );
    assert( isZero( dot( b , cross(a,b) ) ) );

    assert( isZero( cross(a,b) + cross(b,a) ) );

    assert( areEqual( dot(k*a,b) , dot(b,k*a) ) );
    assert( areEqual( cross(a,b+4*c) , cross(a,b)+4*cross(a,c) ) );
    assert( areEqual( cross(2*a,2*b) , 4*cross(a,b) ) );
}

void unitTestRaycasts(){
    Ray r( Point3(0,0,0), Vector3(1,0,0) );
    Sphere s( Point3(5,0,0) , 3 );
    Point3 hitPoint;
    Point3 hitNorm;
    Scalar distMax = 100000;

    bool isHit = rayCast(r,s,hitPoint,hitNorm,distMax);
    assert(isHit);
    assert(areEqual( hitPoint, Point3(2,0,0) ));
}

void unitTestRaycastPlane(){
    Ray r( Point3(0,0,0), Vector3(1,0,0) );
    Plane p( Point3(10,0,0), Vector3(-1,0,0) );
    Point3 hitPoint;
    Point3 hitNorm;
    Scalar distMax = 100000;

    bool isHit = rayCast(r,p,hitPoint,hitNorm,distMax);
    assert(isHit);
    assert(areEqual( hitPoint, Point3(10,0,0) ));
}


float currentTime(){
    static float now = 0.0;
    now += 0.005;
    return now;
    //return std::chrono::system_clock().now();
}




void examplesOfSyntax(){
    Vector3 v(0,2,3);

    Vector3 r = v * 4;  //Vector3 r = v.operator *(4);
    r *= 0.25;
    Vector3 w = v + r; //  Vector3 w = v.operator + (r);
    v += w;

    v = r-v;
    v = -w + v;
    Scalar k = dot(v,w);

    v.x = 0.4f;

    Scalar h = v[0];

    // v[6] = 0.3;
}

void unitTestQuaternions(){

    {
    Quaternion rot = Quaternion::fromAngleAxis(180,Vector3(0,1,0));
    Vector3 p(0,0,1);
    Vector3 q = rot.apply(p);
    assert(areEqual(q, Vector3(0,0,-1)));
    }

    {
    Quaternion rot = Quaternion::fromAngleAxis(90,Vector3(0,1,0));
    Vector3 p(3,5,6);
    Vector3 q = rot.apply(p);
    assert(areEqual(q, Vector3(6,5,-3)));
    }


    Quaternion rot = Quaternion::fromAngleAxis(30,Vector3(1,1,3));
    Vector3 p(3,5,6), q = p;
    for (int k=0; k<12; k++) q = rot.apply(q);
    assert(areEqual(q,p));



    {
        Quaternion q = Quaternion::identity();

        Vector3 randomAxis(4,3,1);
        Scalar deg = 90.f;

        Quaternion qRot = Quaternion::fromAngleAxis(deg,randomAxis);

        for (int i = 0; i < 8; i++) {
            q = q * qRot;
        }

        assert(areEquivalent(q, Quaternion::identity()));
        assert(areEqual(q, Quaternion::identity()));
    }


}

void unitTestTransformation(){
    Transform t;
    t.rotate = Quaternion::fromAngleAxis( 43.4f, Vector3(1,-3,-2) );
    t.translate = Vector3(1,3,4);
    t.scale = 5;

    Point3 p(4,10,-13);

    Point3 q = t.transformPoint( p );
    Point3 r = t.inverse().transformPoint( q );

    assert( areEqual(p,r) );

    Transform ti = t;
    ti.invert();
    r = ti.transformPoint( q );
    assert( areEqual(p,r) );

    Transform tA;
    tA.rotate = Quaternion::fromAngleAxis( -13.4f, Vector3(13,4,0) );
    tA.translate = Vector3(0,-1,01);
    tA.scale = 0.23;

    Transform tB = t;

    Transform tAB = tA*tB;
    assert( areEqual(
              tAB.transformPoint(p),
              tA.transformPoint( tB.transformPoint(p) )
            )
          );


}

// Esegue differenti operazioni a seconda del tasto rilevato in input
void manageInput(CameraState& cs, Vector3& movement, Transform& transform, Quaternion& rotation, int& currentPlayerIndex, std::vector<GameObj>& spheres)
{
    char input = _getch();
    const Scalar step = 0.75f;
    const Scalar rotationAngle = 10.0f;
    switch (input) {
    case 'w':
        movement = transform.rotate.apply(Vector3(0, 0, 1)) * step;
        break;
    case 's':
        movement = -transform.rotate.apply(Vector3(0, 0, 1)) * step;
        break;
    case 'a':
        rotation = Quaternion::fromAngleAxis(-rotationAngle, Vector3(0,1,0));
        break;
    case 'd':
        rotation = Quaternion::fromAngleAxis(rotationAngle, Vector3(0, 1, 0));
        break;
    case 't':
        if (cs == CameraState::CameraView)
        {
            movement = -transform.rotate.apply(Vector3(0, 1, 0)) * step;
        }
        else
        {
            movement = transform.rotate.apply(Vector3(0, 1, 0)) * step;
        }
        break;
    case 'g':
        if (cs == CameraState::CameraView)
        {
            movement = transform.rotate.apply(Vector3(0, 1, 0)) * step;
        }
        else
        {
            movement = -transform.rotate.apply(Vector3(0, 1, 0)) * step;
        }
        break;
    case '0': case '1': case'2': case'3': case '4': case '5': case '6': case '7': case '8': case '9':
        if (cs == CameraState::CharacterView)
        {
            //per i motivi indicati nel commento a riga 230 � necessario riportare la rotazione
            //della sfera precedente allo stato originale e modificare la nuova sfera corrente
            rotation = Quaternion::fromAngleAxis(-180.f, Vector3(0, 0, 1));
            spheres[currentPlayerIndex].transform.rotateSphere(rotation);
            rotation = Quaternion::fromAngleAxis(180.f, Vector3(0, 0, 1));
        }
        currentPlayerIndex = (int)input - 48;
        break;
    case ' ':
        if (cs == CameraState::CameraView)
        {
            //poich� le sfere sono "ribaltate" � necessaria una rotazione per rendere consistente il movimento verso
            //l'alto con i tasti t e g
            cs = CameraState::CharacterView;
            rotation = Quaternion::fromAngleAxis(180.f, Vector3(0, 0, 1));
        }
        else
        {
            cs = CameraState::CameraView;
            rotation = Quaternion::fromAngleAxis(-180.f, Vector3(0, 0, 1));
        }
        break;
    }
}

int main() {

    Scene s;
    s.populate(10);
    int currentPlayerIndex = rand() % 10;
    std::cout << "SPAZIO: switch da visione esterna a first person" << std::endl;
    std::cout << "W/S: Muovi la sfera corrente avanti/indietro" << std::endl;
    std::cout << "A/D: ruota la sfera corrente verso sinistra/destra" << std::endl;
    std::cout << "T/G: muovi la sfera corrente verso l'alto/basso" << std::endl;
    CameraState cv = CameraState::CameraView;
    rayCasting(s.toWorld());

    while (true) {
        Vector3 movement = Vector3(0,0,0);
        Quaternion rotation = Quaternion::identity();
        
        manageInput(cv, movement, s.obj[currentPlayerIndex].transform, rotation, currentPlayerIndex, s.obj);

        system("cls");
        if (!isZero(movement))
            s.obj[currentPlayerIndex].transform.moveSphere(movement);
        if (!areEqual(rotation, Quaternion::identity()))
            s.obj[currentPlayerIndex].transform.rotateSphere(rotation);

        std::cout << "SPAZIO: switch da visione esterna a first person" << std::endl;
        std::cout << "W/S: Muovi la sfera corrente avanti/indietro" << std::endl;
        std::cout << "A/D: ruota la sfera verso sinistra/destra" << std::endl;
        std::cout << "T/G: muovi la sfera corrente verso l'alto/basso" << std::endl;
        if (cv == CameraState::CameraView)
            rayCasting(s.toWorld());
        else
        {
            Transform camera = s.obj[currentPlayerIndex].transform;
            rayCasting(s.toView(camera));
        }
    }
}
